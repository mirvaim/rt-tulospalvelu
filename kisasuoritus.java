package rttulospalvelu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Kisasuoritus-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class kisasuoritus {

    private String luokka;
    private String kokonaispisteet;
    private String suoritusaika;
    private String sijoitus;
    private String koulutustunnus;
    private String keskeytysTaiHylkays;
    private int suoritusID;
    private int kisaID;
    private int koiraID;

    /**
     * @return Palauttaa luokan
     */
    public String getLuokka() {
        return luokka;
    }

    /**
     * @param aLuokka Luokka
     */
    public void setLuokka(String aLuokka) {
        luokka = aLuokka;
    }

    /**
     * @return Palauttaa kokonaispisteet
     */
    public String getKokonaispisteet() {
        return kokonaispisteet;
    }

    /**
     * @param aKokonaispisteet Kokonaispisteet
     */
    public void setKokonaispisteet(String aKokonaispisteet) {
        kokonaispisteet = aKokonaispisteet;
    }

    /**
     * @return Palauttaa suoritusajan
     */
    public String getSuoritusaika() {
        return suoritusaika;
    }

    /**
     * @param aSuoritusaika Suoritusaika
     */
    public void setSuoritusaika(String aSuoritusaika) {
        suoritusaika = aSuoritusaika;
    }

    /**
     * @return Palauttaa sijoituksen
     */
    public String getSijoitus() {
        return sijoitus;
    }

    /**
     * @param aSijoitus Sijoitus
     */
    public void setSijoitus(String aSijoitus) {
        sijoitus = aSijoitus;
    }

    /**
     * @return Palauttaa koulutustunnuksen
     */
    public String getKoulutustunnus() {
        return koulutustunnus;
    }

    /**
     * @param aKoulutustunnus Koulutustunnus
     */
    public void setKoulutustunnus(String aKoulutustunnus) {
        koulutustunnus = aKoulutustunnus;
    }

    /**
     * @return Palauttaa keskeytys tai hylkäys
     */
    public String getKeskeytysTaiHylkays() {
        return keskeytysTaiHylkays;
    }

    /**
     * @param aKeskeytysTaiHylkays Keskeytys tai hylkäys
     */
    public void setKeskeytysTaiHylkays(String aKeskeytysTaiHylkays) {
        keskeytysTaiHylkays = aKeskeytysTaiHylkays;
    }

    /**
     * @return Palauttaa suorituksen ID:n
     */
    public int getSuoritusID() {
        return suoritusID;
    }

    /**
     * @param aSuoritusID Suorituksen ID
     */
    public void setSuoritusID(int aSuoritusID) {
        suoritusID = aSuoritusID;
    }

    /**
     * @return Palauttaa kisan ID:n
     */
    public int getKisaID() {
        return kisaID;
    }

    /**
     * @param aKisaID Kisan ID
     */
    public void setKisaID(int aKisaID) {
        kisaID = aKisaID;
    }

    /**
     * @return Palauttaa koiran ID:n
     */
    public int getKoiraID() {
        return koiraID;
    }

    /**
     * @param aKoiraID Koiran ID
     */
    public void setKoiraID(int aKoiraID) {
        koiraID = aKoiraID;
    }

    @Override
    public String toString() {
        return (suoritusID + " " + luokka + " " + kokonaispisteet + " " + suoritusaika + " " + sijoitus + " " + koulutustunnus + " " + keskeytysTaiHylkays + " " + kisaID + " " + koiraID);
    }

    public static kisasuoritus haeKilpailusuoritus(Connection connection, int id) throws SQLException, Exception {

        String sql = "SELECT suoritusID, luokka, kokonaispisteet, suoritusaika, sijoitus, koulutustunnus, keskeytysTaiHylkays, kisaID, koiraID "
                + " FROM kisasuoritus WHERE suoritusID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, id);

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko == null) {
                throw new Exception("Kilpailusuoritusta ei loydy");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        kisasuoritus kisasuoritusOlio = new kisasuoritus();

        try {
            if (tulosjoukko.next() == true) {

                kisasuoritusOlio.setSuoritusID(tulosjoukko.getInt("suoritusID"));
                kisasuoritusOlio.setLuokka(tulosjoukko.getString("luokka"));
                kisasuoritusOlio.setKokonaispisteet(tulosjoukko.getString("kokonaispisteet"));
                kisasuoritusOlio.setSuoritusaika(tulosjoukko.getString("suoritusaika"));
                kisasuoritusOlio.setSijoitus(tulosjoukko.getString("sijoitus"));
                kisasuoritusOlio.setKoulutustunnus(tulosjoukko.getString("koulutustunnus"));
                kisasuoritusOlio.setKeskeytysTaiHylkays(tulosjoukko.getString("keskeytysTaiHylkays"));
                kisasuoritusOlio.setKisaID(tulosjoukko.getInt("kisaID"));
                kisasuoritusOlio.setKoiraID(tulosjoukko.getInt("koiraID"));
            }

        } catch (SQLException e) {
            throw e;
        }

        return kisasuoritusOlio;
    }

    public int lisaaKilpailusuoritus(Connection connection) throws SQLException, Exception {

        String sql = "SELECT suoritusID"
                + " FROM kisasuoritus WHERE suoritusID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;

        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, getSuoritusID());

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == true) {
                throw new Exception("Kilpailusuoritus on jo olemassa");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        sql = "INSERT INTO kisasuoritus "
                + "(suoritusID, luokka, kokonaispisteet, suoritusaika, sijoitus, koulutustunnus, keskeytysTaiHylkays, kisaID, koiraID) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        lause = null;
        try {

            lause = connection.prepareStatement(sql);

            lause.setInt(1, getSuoritusID());
            lause.setString(2, getLuokka());
            lause.setString(3, getKokonaispisteet());
            lause.setString(4, getSuoritusaika());
            lause.setString(5, getSijoitus());
            lause.setString(6, getKoulutustunnus());
            lause.setString(7, getKeskeytysTaiHylkays());
            lause.setInt(8, getKisaID());
            lause.setInt(9, getKoiraID());

            int lkm = lause.executeUpdate();

            if (lkm == 0) {
                throw new Exception("Kilpailusuorituksen lisaaminen ei onnistu");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }
        return 0;
    }

    public int muutaKilpailusuoritusta(Connection connection) throws SQLException, Exception {

        String sql = "SELECT suoritusID"
                + " FROM kisasuoritus WHERE suoritusID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, getSuoritusID());

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == false) {
                throw new Exception("Kilpailusuoritusta ei loydy tietokannasta");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        sql = "UPDATE kisasuoritus "
                + "SET luokka = ?, kokonaispisteet = ?, suoritusaika = ?, sijoitus = ?, koulutustunnus = ?, keskeytysTaiHylkays = ?, kisaID = ?, koiraID = ? "
                + " WHERE suoritusID = ?";

        lause = null;
        try {

            lause = connection.prepareStatement(sql);

            lause.setString(1, getLuokka());
            lause.setString(2, getKokonaispisteet());
            lause.setString(3, getSuoritusaika());
            lause.setString(4, getSijoitus());
            lause.setString(5, getKoulutustunnus());
            lause.setString(6, getKeskeytysTaiHylkays());
            lause.setInt(7, getKisaID());
            lause.setInt(8, getKoiraID());

            lause.setInt(9, getSuoritusID());

            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Kilpailusuorituksen muuttaminen ei onnistu");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }
        return 0;
    }

    public int poistaKilpailusuoritus(Connection connection) throws SQLException, Exception {

        String sql = "DELETE FROM kisasuoritus WHERE suoritusID = ?";
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);

            lause.setInt(1, getSuoritusID());

            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Kilpailusuorituksen poistaminen ei onnistu");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }
        return 0;
    }

}

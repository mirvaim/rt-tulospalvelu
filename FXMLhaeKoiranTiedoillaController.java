package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Hae kisojen tuloksia koiran tiedoilla Controller
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class FXMLhaeKoiranTiedoillaController implements Initializable {

    private koira m_koira = new koira();
    private Connection m_conn;

    @FXML
    private TextField txtKoiranID;
    @FXML
    private TextField txtVirallinenNimi;
    @FXML
    private TextField txtRekNro;
    @FXML
    private TextField txtSyntaika;
    @FXML
    private TableView<kisasuoritus> tblSuoritukset;
    @FXML
    private TableColumn<kisasuoritus, Integer> col1_suoritusID;
    @FXML
    private TableColumn<kisasuoritus, String> col2_luokka;
    @FXML
    private TableColumn<kisasuoritus, String> col3_kokPisteet;
    @FXML
    private TableColumn<kisasuoritus, String> col4_suoritusAika;
    @FXML
    private TableColumn<kisasuoritus, String> col5_sijoitus;
    @FXML
    private TableColumn<kisasuoritus, String> col6_koulutustunnus;
    @FXML
    private TableColumn<kisasuoritus, String> col7_keskTaiHyl;
    @FXML
    private TableColumn<kisasuoritus, Integer> col8_kisanID;
    @FXML
    private TableColumn<kisasuoritus, Integer> col9_koiranID;
    @FXML
    private TextField txtRotu;
    @FXML
    private TextField txtSakakorkeusluokka;
    @FXML
    private TextField txtOmistajanID;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {

            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }

        //TableView
        col1_suoritusID.setCellValueFactory(new PropertyValueFactory<kisasuoritus, Integer>("suoritusID"));
        col2_luokka.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("luokka"));
        col3_kokPisteet.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("kokonaispisteet"));
        col4_suoritusAika.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("suoritusaika"));
        col5_sijoitus.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("sijoitus"));
        col6_koulutustunnus.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("koulutustunnus"));
        col7_keskTaiHyl.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("keskeytysTaiHylkays"));
        col8_kisanID.setCellValueFactory(new PropertyValueFactory<kisasuoritus, Integer>("kisaID"));
        col9_koiranID.setCellValueFactory(new PropertyValueFactory<kisasuoritus, Integer>("koiraID"));
    }

    public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = "";
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_listaaSuoritukset(ActionEvent event) {
        haeTiedot();
    }

    public void haeTiedot() {
        //tyhjentää edellisen haun tulokset taulukosta
        tblSuoritukset.getItems().clear();

        m_koira = null;
        ArrayList<kisasuoritus> lstKisasuoritukset = null;

        try {
            m_koira = koira.haeKoira(m_conn, Integer.parseInt(txtKoiranID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

        }
        if (m_koira.getVirallinenNimi() == null) {

            txtKoiranID.setText("");
            txtVirallinenNimi.setText("");
            txtRekNro.setText("");
            txtSyntaika.setText("");
            txtRotu.setText("");
            txtSakakorkeusluokka.setText("");
            txtOmistajanID.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

        } else {

            txtKoiranID.setText(String.valueOf(m_koira.getKoiraID()));
            txtVirallinenNimi.setText(m_koira.getVirallinenNimi());
            txtRekNro.setText(m_koira.getRekNro());
            txtSyntaika.setText(m_koira.getSyntymaaika());
            txtRotu.setText(m_koira.getRotu());
            txtSakakorkeusluokka.setText(m_koira.getSakakorkeusluokka());
            txtOmistajanID.setText(String.valueOf(m_koira.getOmistajaID()));
        }

        try {

            lstKisasuoritukset = haeKisasuoritusLista(m_koira.getKoiraID());
        } catch (SQLException se) {

            se.printStackTrace();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            for (kisasuoritus kisaSuoritukset : lstKisasuoritukset) {
                tblSuoritukset.getItems().add(kisaSuoritukset);
            }
        }

    }

    public ArrayList<kisasuoritus> haeKisasuoritusLista(int id) throws SQLException, Exception {
        /**
         * @param id (koiran tunnus)
         * @return kisasuoritusLista
         */
        String sql = "SELECT suoritusID, luokka, kokonaispisteet, suoritusaika, sijoitus, koulutustunnus, keskeytysTaiHylkays, kisaID, koiraID "
                + " FROM kisasuoritus WHERE koiraID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;

        try {

            lause = m_conn.prepareStatement(sql);
            lause.setInt(1, id);
            tulosjoukko = lause.executeQuery();

        } catch (SQLException se) {
            throw se;
        } catch (Exception e) {
            throw e;
        }

        ArrayList<kisasuoritus> kisasuoritusLista = new ArrayList<kisasuoritus>();

        try {

            while (tulosjoukko.next()) {

                kisasuoritus kisasuoritusOlio = new kisasuoritus();
                kisasuoritusOlio.setSuoritusID(tulosjoukko.getInt("suoritusID"));
                kisasuoritusOlio.setLuokka(tulosjoukko.getString("luokka"));
                kisasuoritusOlio.setKokonaispisteet(tulosjoukko.getString("kokonaispisteet"));
                kisasuoritusOlio.setSuoritusaika(tulosjoukko.getString("suoritusaika"));
                kisasuoritusOlio.setSijoitus(tulosjoukko.getString("sijoitus"));
                kisasuoritusOlio.setKoulutustunnus(tulosjoukko.getString("koulutustunnus"));
                kisasuoritusOlio.setKeskeytysTaiHylkays(tulosjoukko.getString("keskeytysTaiHylkays"));
                kisasuoritusOlio.setKisaID(tulosjoukko.getInt("kisaID"));
                kisasuoritusOlio.setKoiraID(tulosjoukko.getInt("koiraID"));

                kisasuoritusLista.add(kisasuoritusOlio);

            }

        } catch (SQLException e) {
            throw e;
        }

        return kisasuoritusLista;

    }

}

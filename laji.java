package rttulospalvelu;

/**
 * Laji-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class laji {

    private static String lajinNimi;
    private static int lajinID;

    /**
     * @return Palauttaa lajin nimen
     */
    public static String getLajinNimi() {
        return lajinNimi;
    }

    /**
     * @param aLajinNimi Lajin nimi
     */
    public static void setLajinNimi(String aLajinNimi) {
        lajinNimi = aLajinNimi;
    }

    /**
     * @return Palauttaa lajin ID:n
     */
    public static int getLajinID() {
        return lajinID;
    }

    /**
     * @param aLajinID Lajin ID
     */
    public static void setLajinID(int aLajinID) {
        lajinID = aLajinID;
    }
}

package rttulospalvelu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Järjestäjä-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class jarjestaja {

    private int jarjestajaID;
    private String seuranNimi;
    private String koetoimitsija;
    private String kennelpiiri;

    /**
     * @return Palauttaa jarjestäjän ID:n
     */
    public int getJarjestajaID() {
        return jarjestajaID;
    }

    /**
     * @param aJarjestajaID Järjestäjän ID
     */
    public void setJarjestajaID(int aJarjestajaID) {
        jarjestajaID = aJarjestajaID;
    }

    /**
     * @return Palauttaa seuran nimen
     */
    public String getSeuranNimi() {
        return seuranNimi;
    }

    /**
     * @param aSeuranNimi Seuran nimi
     */
    public void setSeuranNimi(String aSeuranNimi) {
        seuranNimi = aSeuranNimi;
    }

    /**
     * @return Palauttaa koetoimitsijan
     */
    public String getKoetoimitsija() {
        return koetoimitsija;
    }

    /**
     * @param aKoetoimitsija Koetoimitsija
     */
    public void setKoetoimitsija(String aKoetoimitsija) {
        koetoimitsija = aKoetoimitsija;
    }

    /**
     * @return Palauttaa kennelpiirin
     */
    public String getKennelpiiri() {
        return kennelpiiri;
    }

    /**
     * @param aKennelpiiri Kennelpiiri
     */
    public void setKennelpiiri(String aKennelpiiri) {
        kennelpiiri = aKennelpiiri;
    }
    
    @Override
    public String toString(){
        return (jarjestajaID + " " + seuranNimi + " " + koetoimitsija + " " + kennelpiiri);
    }

    /**
     *
     * @param connection
     * @param id
     * @return
     * @throws SQLException
     * @throws Exception
     */

	public static jarjestaja haeJarjestaja (Connection connection, int id) throws SQLException, Exception { 
		// haetaan tietokannasta järjestäjän ID:llä 
		String sql = "SELECT jarjestajaID, seuranNimi, koetoimitsija, kennelpiiri " 
					+ " FROM jarjestaja WHERE jarjestajaID = ?"; 
		ResultSet tulosjoukko = null;
		PreparedStatement lause = null;
		try {
			lause = connection.prepareStatement(sql);
			lause.setInt( 1, id); 
			
			tulosjoukko = lause.executeQuery();	
			if (tulosjoukko == null) {
				throw new Exception("Järjestäjää ei loydy");
			}
		} catch (SQLException se) {
           
                        throw se;
                } catch (Exception e) {
            
                        throw e;
		}
		
		jarjestaja jarjestajaOlio = new jarjestaja();
		
		try {
			if (tulosjoukko.next () == true){
				
				jarjestajaOlio.setJarjestajaID(tulosjoukko.getInt("jarjestajaID"));
				jarjestajaOlio.setSeuranNimi(tulosjoukko.getString("seuranNimi"));
				jarjestajaOlio.setKoetoimitsija(tulosjoukko.getString("koetoimitsija"));
				jarjestajaOlio.setKennelpiiri(tulosjoukko.getString("kennelpiiri"));
			}
			
		}catch (SQLException e) {
			throw e;
		}
		
		
		return jarjestajaOlio;
	}
      

    /**
     *
     * @param connection
     * @return
     * @throws SQLException
     * @throws Exception
     */

     public int lisaaJarjestaja (Connection connection) throws SQLException, Exception { 

		String sql = "SELECT jarjestajaID" 
					+ " FROM jarjestaja WHERE jarjestajaID = ?"; 
		ResultSet tulosjoukko = null;
		PreparedStatement lause = null; 
		
		try {
			
			lause = connection.prepareStatement(sql);
			lause.setInt( 1, getJarjestajaID()); 
			
			tulosjoukko = lause.executeQuery();	
			if (tulosjoukko.next () == true) {
				throw new Exception("Järjestäjä on jo olemassa");
			}
		} catch (SQLException se) {
            
                    throw se;
                } catch (Exception e) {
            
                    throw e;
		}
		
		sql = "INSERT INTO jarjestaja "
		+ "(jarjestajaID, seuranNimi, koetoimitsija, kennelpiiri) "
		+ " VALUES (?, ?, ?, ?)";
		
		lause = null;
		try {
			
			lause = connection.prepareStatement(sql);
			
			lause.setInt( 1, getJarjestajaID());
			lause.setString(2, getSeuranNimi()); 
			lause.setString(3, getKoetoimitsija()); 
			lause.setString(4, getKennelpiiri());
			
			int lkm = lause.executeUpdate();	
		//	
			if (lkm == 0) {
				throw new Exception("Järjestäjän lisaaminen ei onnistu");
			}
		} catch (SQLException se) {
            
            throw se;
        } catch (Exception e) {
            
            throw e;
		}
		return 0;
	}
     
    /**
     *
     * @param connection
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public int muutaJarjestaja (Connection connection) throws SQLException, Exception { 
		
		String sql = "SELECT jarjestajaID" 
					+ " FROM jarjestaja WHERE jarjestajaID = ?"; 
		ResultSet tulosjoukko = null;
		PreparedStatement lause = null;
		try {
			
			lause = connection.prepareStatement(sql);
			lause.setInt( 1, getJarjestajaID());
			
			tulosjoukko = lause.executeQuery();	
			if (tulosjoukko.next () == false) { 
				throw new Exception("Järjestäjää ei loydy tietokannasta");
			}
		} catch (SQLException se) {
            
                    throw se;
                } catch (Exception e) {
           
                    throw e;
		}
		
		sql = "UPDATE jarjestaja "
		+ "SET seuranNimi = ?, koetoimitsija = ?, kennelpiiri = ? "
		+ " WHERE jarjestajaID = ?";
		
		lause = null;
		try {
			
			lause = connection.prepareStatement(sql);
			
			
			
			lause.setString(1, getSeuranNimi()); 
			lause.setString(2, getKoetoimitsija()); 
			lause.setString(3, getKennelpiiri());
			
            lause.setInt( 4, getJarjestajaID());
			
			int lkm = lause.executeUpdate();	
			if (lkm == 0) {
				throw new Exception("Järjestäjän muuttaminen ei onnistu");
			}
		} catch (SQLException se) {
            
                throw se;
        } catch (Exception e) {
            
                throw e;
		}
		return 0; 
	}
     

    /**
     *
     * @param connection
     * @return
     * @throws SQLException
     * @throws Exception
     */

	public int poistaJarjestaja (Connection connection) throws SQLException, Exception { 
		
		
		String sql = "DELETE FROM jarjestaja WHERE jarjestajaID = ?";
		PreparedStatement lause = null;
		try {
			
			lause = connection.prepareStatement(sql);
			
			lause.setInt( 1, getJarjestajaID());
			
			int lkm = lause.executeUpdate();	
			if (lkm == 0) {
				throw new Exception("Järjestäjän poistaminen ei onnistu");
			}
			} catch (SQLException se) {
            
                throw se;
             } catch (Exception e) {
            
                throw e;
		}
		return 0; 
	}
    
    
}

package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Kilpailu Controller
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class FXMLkilpailuController implements Initializable {

    private kisat m_kisat = new kisat();
    private Connection m_conn;

    @FXML
    private TextField txtKilpailunID;
    @FXML
    private TextField txtPvm;
    @FXML
    private TextField txtOsallistujamaara;
    @FXML
    private TextField txtPaikkakunta;
    @FXML
    private TextField txtTuomari;
    @FXML
    private TextField txtJarjestajaID;
    @FXML
    private TextField txtLajinID;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {
            // JDBC virheet
            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }
    }

    public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = "";
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_lisaaKilpailu(ActionEvent event) {
        lisaaTiedot();
    }

    @FXML
    private void btn_muokkaaKilpailua(ActionEvent event) {
        muutaTiedot();
    }

    @FXML
    private void btn_poistaKilpailu(ActionEvent event) {
        poistaTiedot();
    }

    @FXML
    private void btn_haeKilpailu(ActionEvent event) {
        haeTiedot();
    }

    public void haeTiedot() {

        m_kisat = null;

        try {
            m_kisat = kisat.haeKilpailu(m_conn, Integer.parseInt(txtKilpailunID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailua ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailua ei loydy.");
            alert.showAndWait();

        }
        if (m_kisat.getPvm() == null) {

            txtKilpailunID.setText("");
            txtPvm.setText("");
            txtOsallistujamaara.setText("");
            txtPaikkakunta.setText("");
            txtTuomari.setText("");
            txtJarjestajaID.setText("");
            txtLajinID.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailua ei loydy.");
            alert.showAndWait();

        } else {

            txtKilpailunID.setText(String.valueOf(m_kisat.getKisaID()));
            txtPvm.setText(m_kisat.getPvm());
            txtOsallistujamaara.setText(m_kisat.getOsallistujamaara());
            txtPaikkakunta.setText(m_kisat.getPaikkakunta());
            txtTuomari.setText(m_kisat.getTuomari());
            txtJarjestajaID.setText(String.valueOf(m_kisat.getJarjestajaID()));
            txtLajinID.setText(String.valueOf(m_kisat.getLajinID()));
        }

    }

    public void lisaaTiedot() {

        boolean kisa_lisatty = true;
        m_kisat = null;
        try {
            m_kisat = kisat.haeKilpailu(m_conn, Integer.parseInt(txtKilpailunID.getText()));
        } catch (SQLException se) {

            kisa_lisatty = false;
            se.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailun tietojen lisääminen ei onnistu.");
            alert.showAndWait();

        } catch (Exception e) {

            kisa_lisatty = false;
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailun tietojen lisääminen ei onnistu.");
            alert.showAndWait();
        }
        if (m_kisat.getPvm() != null) {

            kisa_lisatty = false;
            txtKilpailunID.setText(String.valueOf(m_kisat.getKisaID()));
            txtPvm.setText(m_kisat.getPvm());
            txtOsallistujamaara.setText(m_kisat.getOsallistujamaara());
            txtPaikkakunta.setText(m_kisat.getPaikkakunta());
            txtTuomari.setText(m_kisat.getTuomari());
            txtJarjestajaID.setText(String.valueOf(m_kisat.getJarjestajaID()));
            txtLajinID.setText(String.valueOf(m_kisat.getLajinID()));

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen lisaaminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailu on jo olemassa.");
            alert.showAndWait();

        } else {

            m_kisat.setKisaID(Integer.parseInt(txtKilpailunID.getText()));
            m_kisat.setPvm(txtPvm.getText());
            m_kisat.setOsallistujamaara(txtOsallistujamaara.getText());
            m_kisat.setPaikkakunta(txtPaikkakunta.getText());
            m_kisat.setTuomari(txtTuomari.getText());
            m_kisat.setJarjestajaID(Integer.parseInt(txtJarjestajaID.getText()));
            m_kisat.setLajinID(Integer.parseInt(txtLajinID.getText()));
            try {

                m_kisat.lisaaKilpailu(m_conn);
            } catch (SQLException se) {

                kisa_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Kilpailun tietojen lisääminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Kilpailun tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                se.printStackTrace();
            } catch (Exception e) {

                kisa_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Kilpailun tietojen lisaaminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Kilpailun tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                e.printStackTrace();
            } finally {
                if (kisa_lisatty == true) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Kilpailun tietojen lisaaminen");
                    alert.setHeaderText("Toiminto ok.");
                    alert.setContentText("Kilpailun tiedot lisatty tietokantaan.");
                    alert.showAndWait();

                }
            }

        }

    }

    public void muutaTiedot() {

        boolean kisa_muutettu = true;

        m_kisat.setPvm(txtPvm.getText());
        m_kisat.setOsallistujamaara(txtOsallistujamaara.getText());
        m_kisat.setPaikkakunta(txtPaikkakunta.getText());
        m_kisat.setTuomari(txtTuomari.getText());
        m_kisat.setJarjestajaID(Integer.parseInt(txtJarjestajaID.getText()));
        m_kisat.setLajinID(Integer.parseInt(txtLajinID.getText()));

        try {

            m_kisat.muutaKilpailua(m_conn);
        } catch (SQLException se) {

            kisa_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailun tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            kisa_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailun tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (kisa_muutettu == true) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Kilpailun tietojen muuttaminen");
                alert.setHeaderText("Toiminto ok.");
                alert.setContentText("Kilpailun tiedot muutettu.");
                alert.showAndWait();

            }
        }

    }

    public void poistaTiedot() {

        m_kisat = null;
        boolean kisa_poistettu = false;

        try {
            m_kisat = kisat.haeKilpailu(m_conn, Integer.parseInt(txtKilpailunID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailun tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailun tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        }
        if (m_kisat.getPvm() == null) {

            txtPvm.setText("");
            txtOsallistujamaara.setText("");
            txtPaikkakunta.setText("");
            txtTuomari.setText("");
            txtJarjestajaID.setText("");
            txtLajinID.setText("");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen poisto");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailua ei loydy.");
            alert.showAndWait();

            return;
        } else {

            txtPvm.setText(m_kisat.getPvm());
            txtOsallistujamaara.setText(m_kisat.getOsallistujamaara());
            txtPaikkakunta.setText(m_kisat.getPaikkakunta());
            txtPvm.setText(m_kisat.getPvm());
            txtJarjestajaID.setText(String.valueOf(m_kisat.getJarjestajaID()));
            txtLajinID.setText(String.valueOf(m_kisat.getLajinID()));
        }
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Kilpailun tietojen poisto");
            alert.setHeaderText("Vahvista");
            alert.setContentText("Haluatko todella poistaa kilpailun?");

            Optional<ButtonType> vastaus = alert.showAndWait();

            if (vastaus.get() == ButtonType.OK) {
                m_kisat.poistaKilpailu(m_conn);
                kisa_poistettu = true;
            }
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Kilpailun tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Kilpailun tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (kisa_poistettu == true) {
                txtKilpailunID.setText("");
                txtPvm.setText("");
                txtOsallistujamaara.setText("");
                txtPaikkakunta.setText("");
                txtTuomari.setText("");
                txtJarjestajaID.setText("");
                txtLajinID.setText("");

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Kilpailun tietojen poisto");
                alert.setHeaderText("Tulos:");
                alert.setContentText("Kilpailun tiedot poistettu tietokannasta.");
                alert.showAndWait();

                m_kisat = null;
            }
        }

    }

}

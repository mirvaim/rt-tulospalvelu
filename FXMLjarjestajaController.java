package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Järjestäjä Controller
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class FXMLjarjestajaController implements Initializable {

    private jarjestaja m_jarjestaja = new jarjestaja();
    private Connection m_conn;

    @FXML
    private TextField txtJarjestajaID;
    @FXML
    private TextField txtSeuranNimi;
    @FXML
    private TextField txtKoetoimistija;
    @FXML
    private TextField txtKennelpiiri;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {

            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }

    }

    public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = ""; 
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_lisaaJarjestaja(ActionEvent event) {
        lisaaTiedot();
    }

    @FXML
    private void btn_muokkaaJarjestaja(ActionEvent event) {
        muutaTiedot();
    }

    @FXML
    private void btn_poistaJarjestaja(ActionEvent event) {
        poistaTiedot();
    }

    @FXML
    private void btn_haeJarjestaja(ActionEvent event) {
        haeTiedot();
    }

    public void haeTiedot() {

        m_jarjestaja = null;

        try {
            m_jarjestaja = jarjestaja.haeJarjestaja(m_conn, Integer.parseInt(txtJarjestajaID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Järjestäjää ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Järjestäjää ei loydy.");
            alert.showAndWait();

        }
        if (m_jarjestaja.getSeuranNimi() == null) {

            txtJarjestajaID.setText("");
            txtSeuranNimi.setText("");
            txtKoetoimistija.setText("");
            txtKennelpiiri.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Järjestäjää ei loydy.");
            alert.showAndWait();

        } else {

            txtJarjestajaID.setText(String.valueOf(m_jarjestaja.getJarjestajaID()));
            txtSeuranNimi.setText(m_jarjestaja.getSeuranNimi());
            txtKoetoimistija.setText(m_jarjestaja.getKoetoimitsija());
            txtKennelpiiri.setText(m_jarjestaja.getKennelpiiri());
        }

    }

    public void lisaaTiedot() {

        boolean jarjestaja_lisatty = true;
        m_jarjestaja = null;
        try {
            m_jarjestaja = jarjestaja.haeJarjestaja(m_conn, Integer.parseInt(txtJarjestajaID.getText()));
        } catch (SQLException se) {

            jarjestaja_lisatty = false;
            se.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Järjestäjän tietojen lisääminen ei onnistu.");
            alert.showAndWait();

        } catch (Exception e) {

            jarjestaja_lisatty = false;
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Järjestäjän tietojen lisääminen ei onnistu.");
            alert.showAndWait();
        }
        if (m_jarjestaja.getSeuranNimi() != null) {

            jarjestaja_lisatty = false;
            txtJarjestajaID.setText(String.valueOf(m_jarjestaja.getJarjestajaID()));
            txtSeuranNimi.setText(m_jarjestaja.getSeuranNimi());
            txtKoetoimistija.setText(m_jarjestaja.getKoetoimitsija());
            txtKennelpiiri.setText(m_jarjestaja.getKennelpiiri());

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen lisaaminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Järjestäjä on jo olemassa.");
            alert.showAndWait();

        } else {

            m_jarjestaja.setJarjestajaID(Integer.parseInt(txtJarjestajaID.getText()));
            m_jarjestaja.setSeuranNimi(txtSeuranNimi.getText());
            m_jarjestaja.setKoetoimitsija(txtKoetoimistija.getText());
            m_jarjestaja.setKennelpiiri(txtKennelpiiri.getText());
            try {

                m_jarjestaja.lisaaJarjestaja(m_conn);
            } catch (SQLException se) {

                jarjestaja_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Järjestäjän tietojen lisääminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Järjestäjän tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                se.printStackTrace();
            } catch (Exception e) {

                jarjestaja_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Järjestäjän tietojen lisaaminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Järjestäjän tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                e.printStackTrace();
            } finally {
                if (jarjestaja_lisatty == true) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Järjestäjän tietojen lisaaminen");
                    alert.setHeaderText("Toiminto ok.");
                    alert.setContentText("Järjestäjän tiedot lisatty tietokantaan.");
                    alert.showAndWait();

                }
            }

        }

    }

    public void muutaTiedot() {

        boolean jarjestaja_muutettu = true;

        m_jarjestaja.setSeuranNimi(txtSeuranNimi.getText());
        m_jarjestaja.setKoetoimitsija(txtKoetoimistija.getText());
        m_jarjestaja.setKennelpiiri(txtKennelpiiri.getText());

        try {

            m_jarjestaja.muutaJarjestaja(m_conn);
        } catch (SQLException se) {

            jarjestaja_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Järjestäjän tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            jarjestaja_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Järjestäjän tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (jarjestaja_muutettu == true) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Järjestäjän tietojen muuttaminen");
                alert.setHeaderText("Toiminto ok.");
                alert.setContentText("Järjestäjän tiedot muutettu.");
                alert.showAndWait();

            }
        }

    }

    public void poistaTiedot() {

        m_jarjestaja = null;
        boolean jarjestaja_poistettu = false;

        try {
            m_jarjestaja = jarjestaja.haeJarjestaja(m_conn, Integer.parseInt(txtJarjestajaID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Järjestäjän tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Järjestäjän tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        }
        if (m_jarjestaja.getSeuranNimi() == null) {

            txtSeuranNimi.setText("");
            txtKoetoimistija.setText("");
            txtKennelpiiri.setText("");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen poisto");
            alert.setHeaderText("Virhe");
            alert.setContentText("Järjestäjää ei loydy.");
            alert.showAndWait();

            return;
        } else {

            txtSeuranNimi.setText(m_jarjestaja.getSeuranNimi());
            txtKoetoimistija.setText(m_jarjestaja.getKoetoimitsija());
            txtKennelpiiri.setText(m_jarjestaja.getKennelpiiri());
        }
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Järjestäjän tietojen poisto");
            alert.setHeaderText("Vahvista");
            alert.setContentText("Haluatko todella poistaa järjestäjän?");

            Optional<ButtonType> vastaus = alert.showAndWait();

            if (vastaus.get() == ButtonType.OK) {
                m_jarjestaja.poistaJarjestaja(m_conn);
                jarjestaja_poistettu = true;
            }
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Järjestäjän tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Järjestäjän tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Järjestäjän tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (jarjestaja_poistettu == true) {
                txtJarjestajaID.setText("");
                txtSeuranNimi.setText("");
                txtKoetoimistija.setText("");
                txtKennelpiiri.setText("");

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Järjestäjän tietojen poisto");
                alert.setHeaderText("Tulos:");
                alert.setContentText("Järjestäjän tiedot poistettu tietokannasta.");
                alert.showAndWait();

                m_jarjestaja = null;
            }
        }

    }

}

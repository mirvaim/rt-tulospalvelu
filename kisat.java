package rttulospalvelu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Kisat-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class kisat {

    private int kisaID;
    private String pvm;
    private String osallistujamaara;
    private String paikkakunta;
    private String tuomari;
    private int jarjestajaID;
    private int lajinID;

    /**
     * @return Palauttaa kisan ID:n
     */
    public int getKisaID() {
        return kisaID;
    }

    /**
     * @param aKisaID Kisan ID
     */
    public void setKisaID(int aKisaID) {
        kisaID = aKisaID;
    }

    /**
     * @return Palauttaa päivämäärän
     */
    public String getPvm() {
        return pvm;
    }

    /**
     * @param aPvm Päivämäärä
     */
    public void setPvm(String aPvm) {
        pvm = aPvm;
    }

    /**
     * @return Palauttaa osallistujamäärän
     */
    public String getOsallistujamaara() {
        return osallistujamaara;
    }

    /**
     * @param aOsallistujamaara Osallistujamäärä
     */
    public void setOsallistujamaara(String aOsallistujamaara) {
        osallistujamaara = aOsallistujamaara;
    }

    /**
     * @return Palauttaa paikkakunnan
     */
    public String getPaikkakunta() {
        return paikkakunta;
    }

    /**
     * @param aPaikkakunta Paikkakunta
     */
    public void setPaikkakunta(String aPaikkakunta) {
        paikkakunta = aPaikkakunta;
    }

    /**
     * @return Palauttaa tuomarin
     */
    public String getTuomari() {
        return tuomari;
    }

    /**
     * @param aTuomari Tuomari
     */
    public void setTuomari(String aTuomari) {
        tuomari = aTuomari;
    }

    /**
     * @return Palauttaa järjestäjän ID:n
     */
    public int getJarjestajaID() {
        return jarjestajaID;
    }

    /**
     * @param aJarjestajaID Järjestäjän ID
     */
    public void setJarjestajaID(int aJarjestajaID) {
        jarjestajaID = aJarjestajaID;
    }

    /**
     * @return Palauttaa lajin ID:n
     */
    public int getLajinID() {
        return lajinID;
    }

    /**
     * @param aLajinID Lajin ID
     */
    public void setLajinID(int aLajinID) {
        lajinID = aLajinID;
    }

    @Override
    public String toString() {
        return (kisaID + " " + pvm + " " + osallistujamaara + " " + paikkakunta + " " + tuomari + " " + jarjestajaID + " " + lajinID);
    }

    public static kisat haeKilpailu(Connection connection, int id) throws SQLException, Exception {

        String sql = "SELECT * "
                + " FROM kisat WHERE kisaID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, id);

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko == null) {
                throw new Exception("Kilpailua ei loydy");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        kisat kisaOlio = new kisat();

        try {
            if (tulosjoukko.next() == true) {

                kisaOlio.setKisaID(tulosjoukko.getInt("kisaID"));
                kisaOlio.setPvm(tulosjoukko.getString("pvm"));
                kisaOlio.setOsallistujamaara(tulosjoukko.getString("osallistujamaara"));
                kisaOlio.setPaikkakunta(tulosjoukko.getString("paikkakunta"));
                kisaOlio.setTuomari(tulosjoukko.getString("tuomari"));
                kisaOlio.setJarjestajaID(tulosjoukko.getInt("jarjestajaID"));
                kisaOlio.setLajinID(tulosjoukko.getInt("lajinID"));
            }

        } catch (SQLException e) {
            throw e;
        }

        return kisaOlio;
    }

    public int lisaaKilpailu(Connection connection) throws SQLException, Exception {

        String sql = "SELECT kisaID"
                + " FROM kisat WHERE kisaID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;

        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, getKisaID());

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == true) {
                throw new Exception("Kilpailu on jo olemassa");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        sql = "INSERT INTO kisat "
                + "(kisaID, pvm, osallistujamaara, paikkakunta, tuomari, jarjestajaID, lajinID) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?)";

        lause = null;
        try {

            lause = connection.prepareStatement(sql);

            lause.setInt(1, getKisaID());
            lause.setString(2, getPvm());
            lause.setString(3, getOsallistujamaara());
            lause.setString(4, getPaikkakunta());
            lause.setString(5, getTuomari());
            lause.setInt(6, getJarjestajaID());
            lause.setInt(7, getLajinID());

            int lkm = lause.executeUpdate();

            if (lkm == 0) {
                throw new Exception("Kilpailun lisaaminen ei onnistu");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }
        return 0;
    }

    public int muutaKilpailua(Connection connection) throws SQLException, Exception {

        String sql = "SELECT kisaID"
                + " FROM kisat WHERE kisaID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, getKisaID());

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == false) {
                throw new Exception("Kilpailua ei loydy tietokannasta");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        sql = "UPDATE kisat "
                + "SET pvm = ?, osallistujamaara = ?, paikkakunta = ?, tuomari = ?, jarjestajaID = ?, lajinID = ? "
                + " WHERE kisaID = ?";

        lause = null;
        try {

            lause = connection.prepareStatement(sql);

            lause.setString(1, getPvm());
            lause.setString(2, getOsallistujamaara());
            lause.setString(3, getPaikkakunta());
            lause.setString(4, getTuomari());
            lause.setInt(5, getJarjestajaID());
            lause.setInt(6, getLajinID());

            lause.setInt(7, getKisaID());

            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Kilpailun muuttaminen ei onnistu");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }
        return 0;
    }

    public int poistaKilpailu(Connection connection) throws SQLException, Exception {

        String sql = "DELETE FROM kisat WHERE kisaID = ?";
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);

            lause.setInt(1, getKisaID());

            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Kilpailun poistaminen ei onnistu");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }
        return 0;
    }

}

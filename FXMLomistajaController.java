package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Omistaja Controller
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class FXMLomistajaController implements Initializable {

    private omistaja m_omistaja = new omistaja();
    private Connection m_conn;

    @FXML
    private TextField txtOmistajanID;
    @FXML
    private TextField txtEtunimi;
    @FXML
    private TextField txtSukunimi;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {

            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }
    }

    public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = "";
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_lisaaOmistaja(ActionEvent event) {
        lisaaTiedot();
    }

    @FXML
    private void btn_muokkaaOmistajaa(ActionEvent event) {
        muutaTiedot();
    }

    @FXML
    private void btn_poistaOmistaja(ActionEvent event) {
        poistaTiedot();
    }

    @FXML
    private void btn_haeOmistaja(ActionEvent event) {
        haeTiedot();
    }

    public void haeTiedot() {

        m_omistaja = null;

        try {
            m_omistaja = omistaja.haeOmistaja(m_conn, Integer.parseInt(txtOmistajanID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Omistajaa ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Omistajaa ei loydy.");
            alert.showAndWait();

        }
        if (m_omistaja.getEtunimi() == null) {

            txtOmistajanID.setText("");
            txtEtunimi.setText("");
            txtSukunimi.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Omistajaa ei loydy.");
            alert.showAndWait();

        } else {

            txtOmistajanID.setText(String.valueOf(m_omistaja.getOmistajaID()));
            txtEtunimi.setText(m_omistaja.getEtunimi());
            txtSukunimi.setText(m_omistaja.getSukunimi());
        }

    }

    public void lisaaTiedot() {

        boolean omistaja_lisatty = true;
        m_omistaja = null;
        try {
            m_omistaja = omistaja.haeOmistaja(m_conn, Integer.parseInt(txtOmistajanID.getText()));
        } catch (SQLException se) {

            omistaja_lisatty = false;
            se.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Omistajan tietojen lisääminen ei onnistu.");
            alert.showAndWait();

        } catch (Exception e) {

            omistaja_lisatty = false;
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Omistajan tietojen lisääminen ei onnistu.");
            alert.showAndWait();
        }
        if (m_omistaja.getEtunimi() != null) {

            omistaja_lisatty = false;
            txtOmistajanID.setText(String.valueOf(m_omistaja.getOmistajaID()));
            txtEtunimi.setText(m_omistaja.getEtunimi());
            txtSukunimi.setText(m_omistaja.getSukunimi());

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen lisaaminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Omistaja on jo olemassa.");
            alert.showAndWait();

        } else {

            m_omistaja.setOmistajaID(Integer.parseInt(txtOmistajanID.getText()));
            m_omistaja.setEtunimi(txtEtunimi.getText());
            m_omistaja.setSukunimi(txtSukunimi.getText());
            try {

                m_omistaja.lisaaOmistaja(m_conn);
            } catch (SQLException se) {

                omistaja_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Omistajan tietojen lisääminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Omistajan tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                se.printStackTrace();
            } catch (Exception e) {

                omistaja_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Omistajan tietojen lisaaminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Omistajan tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                e.printStackTrace();
            } finally {
                if (omistaja_lisatty == true) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Omistajan tietojen lisaaminen");
                    alert.setHeaderText("Toiminto ok.");
                    alert.setContentText("Omistajan tiedot lisatty tietokantaan.");
                    alert.showAndWait();

                }
            }

        }

    }

    public void muutaTiedot() {

        boolean omistaja_muutettu = true;

        m_omistaja.setEtunimi(txtEtunimi.getText());
        m_omistaja.setSukunimi(txtSukunimi.getText());

        try {

            m_omistaja.muutaOmistaja(m_conn);
        } catch (SQLException se) {

            omistaja_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Omistajan tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            omistaja_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Omistajan tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (omistaja_muutettu == true) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Omistajan tietojen muuttaminen");
                alert.setHeaderText("Toiminto ok.");
                alert.setContentText("Omistajan tiedot muutettu.");
                alert.showAndWait();

            }
        }

    }

    public void poistaTiedot() {

        m_omistaja = null;
        boolean omistaja_poistettu = false;

        try {
            m_omistaja = omistaja.haeOmistaja(m_conn, Integer.parseInt(txtOmistajanID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Omistajan tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Omistajan tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        }
        if (m_omistaja.getEtunimi() == null) {

            txtEtunimi.setText("");
            txtSukunimi.setText("");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen poisto");
            alert.setHeaderText("Virhe");
            alert.setContentText("Omistajan ei loydy.");
            alert.showAndWait();

            return;
        } else {

            txtEtunimi.setText(m_omistaja.getEtunimi());
            txtSukunimi.setText(m_omistaja.getSukunimi());
        }
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Omistajan tietojen poisto");
            alert.setHeaderText("Vahvista");
            alert.setContentText("Haluatko todella poistaa omistajan?");

            Optional<ButtonType> vastaus = alert.showAndWait();

            if (vastaus.get() == ButtonType.OK) {
                m_omistaja.poistaOmistaja(m_conn);
                omistaja_poistettu = true;
            }
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Omistajan tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Omistajan tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Omistajan tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (omistaja_poistettu == true) {
                txtOmistajanID.setText("");
                txtEtunimi.setText("");
                txtSukunimi.setText("");

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Omistajan tietojen poisto");
                alert.setHeaderText("Tulos:");
                alert.setContentText("Omistajan tiedot poistettu tietokannasta.");
                alert.showAndWait();

                m_omistaja = null;
            }
        }

    }

}

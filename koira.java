package rttulospalvelu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Koira-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class koira {

    private int koiraID;
    private String virallinenNimi;
    private String rekNro;
    private String syntymaaika;
    private String rotu;
    private String sakakorkeusluokka;
    private int omistajaID;

    /**
     * @return Palauttaa koiran ID:n
     */
    public int getKoiraID() {
        return koiraID;
    }

    /**
     * @param aKoiraID Koiran ID
     */
    public void setKoiraID(int aKoiraID) {
        koiraID = aKoiraID;
    }

    /**
     * @return Palauttaa virallisen nimen
     */
    public String getVirallinenNimi() {
        return virallinenNimi;
    }

    /**
     * @param aVirallinenNimi Virallinen nimi
     */
    public void setVirallinenNimi(String aVirallinenNimi) {
        virallinenNimi = aVirallinenNimi;
    }

    /**
     * @return Palauttaa rekisterinumeron
     */
    public String getRekNro() {
        return rekNro;
    }

    /**
     * @param aRekNro Rekisterinumero
     */
    public void setRekNro(String aRekNro) {
        rekNro = aRekNro;
    }

    /**
     * @return Palauttaa syntymäajan
     */
    public String getSyntymaaika() {
        return syntymaaika;
    }

    /**
     * @param aSyntymaaika Syntymäaika
     */
    public void setSyntymaaika(String aSyntymaaika) {
        syntymaaika = aSyntymaaika;
    }

    /**
     * @return Palauttaa rodun
     */
    public String getRotu() {
        return rotu;
    }

    /**
     * @param aRotu Rotu
     */
    public void setRotu(String aRotu) {
        rotu = aRotu;
    }

    /**
     * @return Palauttaa säkäkorkeusluokan
     */
    public String getSakakorkeusluokka() {
        return sakakorkeusluokka;
    }

    /**
     * @param aSakakorkeusluokka Säkäkorkeusluokka
     */
    public void setSakakorkeusluokka(String aSakakorkeusluokka) {
        sakakorkeusluokka = aSakakorkeusluokka;
    }

    /**
     * @return Palauttaa omistajan ID:n
     */
    public int getOmistajaID() {
        return omistajaID;
    }

    /**
     * @param aOmistajaID Omistajan ID
     */
    public void setOmistajaID(int aOmistajaID) {
        omistajaID = aOmistajaID;
    }

    @Override
    public String toString() {
        return (koiraID + " " + virallinenNimi + " " + rekNro + " " + syntymaaika + " " + rotu + " " + sakakorkeusluokka + " " + omistajaID);
    }

    public static koira haeKoira(Connection connection, int id) throws SQLException, Exception { 
        
        String sql = "SELECT koiraID, virallinenNimi, rekNro, syntymaaika, rotu, sakakorkeusluokka, omistajaID "
                + " FROM koira WHERE koiraID = ?"; 
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {
    
            lause = connection.prepareStatement(sql);
            lause.setInt(1, id); 
        
            tulosjoukko = lause.executeQuery();
            if (tulosjoukko == null) {
                throw new Exception("Koiraa ei loydy");
            }
        } catch (SQLException se) {
          
            throw se;
        } catch (Exception e) {
        
            throw e;
        }
  
        koira koiraOlio = new koira();

        try {
            if (tulosjoukko.next() == true) {
             
                koiraOlio.setKoiraID(tulosjoukko.getInt("koiraID"));
                koiraOlio.setVirallinenNimi(tulosjoukko.getString("virallinenNimi"));
                koiraOlio.setRekNro(tulosjoukko.getString("rekNro"));
                koiraOlio.setSyntymaaika(tulosjoukko.getString("syntymaaika"));
                koiraOlio.setRotu(tulosjoukko.getString("rotu"));
                koiraOlio.setSakakorkeusluokka(tulosjoukko.getString("sakakorkeusluokka"));
                koiraOlio.setOmistajaID(tulosjoukko.getInt("omistajaID"));
            }

        } catch (SQLException e) {
            throw e;
        }
   

        return koiraOlio;
    }

    public int lisaaKoira(Connection connection) throws SQLException, Exception { 
     
        String sql = "SELECT koiraID"
                + " FROM koira WHERE koiraID = ?"; 
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;

        try {
      
            lause = connection.prepareStatement(sql);
            lause.setInt(1, getKoiraID()); 
       
            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == true) {
                throw new Exception("Koira on jo olemassa");
            }
        } catch (SQLException se) {
         
            throw se;
        } catch (Exception e) {
          
            throw e;
        }
    
        sql = "INSERT INTO koira "
                + "(koiraID, virallinenNimi, rekNro, syntymaaika, rotu, sakakorkeusluokka, omistajaID) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?)";
     
        lause = null;
        try {
          
            lause = connection.prepareStatement(sql);
       
            lause.setInt(1, getKoiraID());
            lause.setString(2, getVirallinenNimi());
            lause.setString(3, getRekNro());
            lause.setString(4, getSyntymaaika());
            lause.setString(5, getRotu());
            lause.setString(6, getSakakorkeusluokka());
            lause.setInt(7, getOmistajaID());
    
            int lkm = lause.executeUpdate();
        
            if (lkm == 0) {
                throw new Exception("Koiran lisaaminen ei onnistu");
            }
        } catch (SQLException se) {
        
            throw se;
        } catch (Exception e) {
          
            throw e;
        }
        return 0;
    }

    public int muutaKoira(Connection connection) throws SQLException, Exception { 
    
        String sql = "SELECT koiraID"
                + " FROM koira WHERE koiraID = ?"; 
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {
     
            lause = connection.prepareStatement(sql);
            lause.setInt(1, getKoiraID()); 
           
            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == false) { 
                throw new Exception("Koiraa ei loydy tietokannasta");
            }
        } catch (SQLException se) {
         
            throw se;
        } catch (Exception e) {
         
            throw e;
        }
       
        sql = "UPDATE koira "
                + "SET virallinenNimi = ?, rekNro = ?, syntymaaika = ?, rotu = ?, sakakorkeusluokka = ?, omistajaID = ? "
                + " WHERE koiraID = ?";

        lause = null;
        try {
        
            lause = connection.prepareStatement(sql);

           
            lause.setString(1, getVirallinenNimi());
            lause.setString(2, getRekNro());
            lause.setString(3, getSyntymaaika());
            lause.setString(4, getRotu());
            lause.setString(5, getSakakorkeusluokka());
            lause.setInt(6, getOmistajaID());
           
            lause.setInt(7, getKoiraID());
          
            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Koiran muuttaminen ei onnistu");
            }
        } catch (SQLException se) {
        
            throw se;
        } catch (Exception e) {
        
            throw e;
        }
        return 0;
    }


    public int poistaKoira(Connection connection) throws SQLException, Exception { 

      
        String sql = "DELETE FROM koira WHERE koiraID = ?";
        PreparedStatement lause = null;
        try {
        
            lause = connection.prepareStatement(sql);
         
            lause.setInt(1, getKoiraID());
       
            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Koiran poistaminen ei onnistu");
            }
        } catch (SQLException se) {
       
            throw se;
        } catch (Exception e) {
        
            throw e;
        }
        return 0; 
    }

}

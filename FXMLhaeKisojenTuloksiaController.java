package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Hae kisojen tuloksia Controller
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class FXMLhaeKisojenTuloksiaController implements Initializable {

    private kisat m_kisat = new kisat(); 
    private Connection m_conn; 

    @FXML
    private TextField txtKilpailunID;
    @FXML
    private TextField txtPvm;
    @FXML
    private TextField txtOsallistujamaara;
    @FXML
    private TextField txtPaikkakunta;
    @FXML
    private TextField txtTuomari;
    @FXML
    private TextField txtJarjestajanID;
    @FXML
    private TextField txtLajinID;
    @FXML
    private TableView<kisasuoritus> tblTulokset;
    @FXML
    private TableColumn<kisasuoritus, Integer> col1_suoritusID;
    @FXML
    private TableColumn<kisasuoritus, String> col2_luokka;
    @FXML
    private TableColumn<kisasuoritus, String> col3_kokonaispisteet;
    @FXML
    private TableColumn<kisasuoritus, String> col4_suoritusaika;
    @FXML
    private TableColumn<kisasuoritus, String> col5_sijoitus;
    @FXML
    private TableColumn<kisasuoritus, String> col6_koulutustunnus;
    @FXML
    private TableColumn<kisasuoritus, String> col7_keskTaiHyl;
    @FXML
    private TableColumn<kisasuoritus, Integer> col8_kisaID;
    @FXML
    private TableColumn<kisasuoritus, Integer> col9_koiraID;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {

            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }

        //TableView
        col1_suoritusID.setCellValueFactory(new PropertyValueFactory<kisasuoritus, Integer>("suoritusID"));
        col2_luokka.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("luokka"));
        col3_kokonaispisteet.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("kokonaispisteet"));
        col4_suoritusaika.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("suoritusaika"));
        col5_sijoitus.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("sijoitus"));
        col6_koulutustunnus.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("koulutustunnus"));
        col7_keskTaiHyl.setCellValueFactory(new PropertyValueFactory<kisasuoritus, String>("keskeytysTaiHylkays"));
        col8_kisaID.setCellValueFactory(new PropertyValueFactory<kisasuoritus, Integer>("kisaID"));
        col9_koiraID.setCellValueFactory(new PropertyValueFactory<kisasuoritus, Integer>("koiraID"));
    }

public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = ""; 
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_listaaTulokset(ActionEvent event) {
        haeTiedot();
    }

    public void haeTiedot() {
        //tyhjentää edellisen haun tulokset taulukosta
        tblTulokset.getItems().clear();

        m_kisat = null;
        ArrayList<kisasuoritus> lstKisasuoritukset = null;

        try {
            m_kisat = kisat.haeKilpailu(m_conn, Integer.parseInt(txtKilpailunID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tulosten hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Tuloksia ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tulosten hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Tuloksia ei loydy.");
            alert.showAndWait();

        }
        if (m_kisat.getPvm() == null) {

            txtKilpailunID.setText("");
            txtPvm.setText("");
            txtOsallistujamaara.setText("");
            txtPaikkakunta.setText("");
            txtTuomari.setText("");
            txtJarjestajanID.setText("");
            txtLajinID.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailun tulosten hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Tuloksia ei loydy.");
            alert.showAndWait();

        } else {

            txtKilpailunID.setText(String.valueOf(m_kisat.getKisaID()));
            txtPvm.setText(m_kisat.getPvm());
            txtOsallistujamaara.setText(m_kisat.getOsallistujamaara());
            txtPaikkakunta.setText(m_kisat.getPaikkakunta());
            txtTuomari.setText(m_kisat.getTuomari());
            txtJarjestajanID.setText(String.valueOf(m_kisat.getJarjestajaID()));
            txtLajinID.setText(String.valueOf(m_kisat.getLajinID()));
        }

        try {

            lstKisasuoritukset = haeKisasuoritusLista(m_kisat.getKisaID());
        } catch (SQLException se) {

            se.printStackTrace();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            for (kisasuoritus kisaSuoritukset : lstKisasuoritukset) {
                tblTulokset.getItems().add(kisaSuoritukset);
            }
        }

    }

    public ArrayList<kisasuoritus> haeKisasuoritusLista(int id) throws SQLException, Exception {
        /**
         * @param id (kisasuorituksen tunnus)
         * @return kisasuoritusLista
         */
        String sql = "SELECT suoritusID, luokka, kokonaispisteet, suoritusaika, sijoitus, koulutustunnus, keskeytysTaiHylkays, kisaID, koiraID "
                + " FROM kisasuoritus WHERE kisaID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;

        try {

            lause = m_conn.prepareStatement(sql);
            lause.setInt(1, id);
            tulosjoukko = lause.executeQuery();

        } catch (SQLException se) {
            throw se;
        } catch (Exception e) {
            throw e;
        }

        ArrayList<kisasuoritus> kisasuoritusLista = new ArrayList<kisasuoritus>();

        try {

            while (tulosjoukko.next()) {

                kisasuoritus kisasuoritusOlio = new kisasuoritus();
                kisasuoritusOlio.setSuoritusID(tulosjoukko.getInt("suoritusID"));
                kisasuoritusOlio.setLuokka(tulosjoukko.getString("luokka"));
                kisasuoritusOlio.setKokonaispisteet(tulosjoukko.getString("kokonaispisteet"));
                kisasuoritusOlio.setSuoritusaika(tulosjoukko.getString("suoritusaika"));
                kisasuoritusOlio.setSijoitus(tulosjoukko.getString("sijoitus"));
                kisasuoritusOlio.setKoulutustunnus(tulosjoukko.getString("koulutustunnus"));
                kisasuoritusOlio.setKeskeytysTaiHylkays(tulosjoukko.getString("keskeytysTaiHylkays"));
                kisasuoritusOlio.setKisaID(tulosjoukko.getInt("kisaID"));
                kisasuoritusOlio.setKoiraID(tulosjoukko.getInt("koiraID"));

                kisasuoritusLista.add(kisasuoritusOlio);

            }

        } catch (SQLException e) {
            throw e;
        }

        return kisasuoritusLista;

    }

}

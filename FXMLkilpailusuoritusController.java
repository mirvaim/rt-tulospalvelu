package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Kilpailusuoritus Controller
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class FXMLkilpailusuoritusController implements Initializable {

    private kisasuoritus m_kisasuoritus = new kisasuoritus();
    private Connection m_conn;

    @FXML
    private TextField txtSuoritusID;
    @FXML
    private TextField txtLuokka;
    @FXML
    private TextField txtKokonaispisteet;
    @FXML
    private TextField txtSuoritusaika;
    @FXML
    private TextField txtSijoitus;
    @FXML
    private TextField txtKoulutustunnus;
    @FXML
    private TextField txtKeskTaiHyl;
    @FXML
    private TextField txtKisanID;
    @FXML
    private TextField txtKoiranID;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {

            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }
    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_lisaaKisasuoritus(ActionEvent event) {
        lisaaTiedot();
    }

    @FXML
    private void btn_muokkaaKisasuoritusta(ActionEvent event) {
        muutaTiedot();
    }

    @FXML
    private void btn_poistaKisasuoritus(ActionEvent event) {
        poistaTiedot();
    }

    @FXML
    private void btn_haeKisasuoritus(ActionEvent event) {
        haeTiedot();
    }

    public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = "";
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void haeTiedot() {

        m_kisasuoritus = null;

        try {
            m_kisasuoritus = kisasuoritus.haeKilpailusuoritus(m_conn, Integer.parseInt(txtSuoritusID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailusuoritusta ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailusuoritusta ei loydy.");
            alert.showAndWait();

        }
        if (m_kisasuoritus.getLuokka() == null) {

            txtSuoritusID.setText("");
            txtLuokka.setText("");
            txtKokonaispisteet.setText("");
            txtSuoritusaika.setText("");
            txtSijoitus.setText("");
            txtKoulutustunnus.setText("");
            txtKeskTaiHyl.setText("");
            txtKisanID.setText("");
            txtKoiranID.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailusuoritusta ei loydy.");
            alert.showAndWait();

        } else {

            txtSuoritusID.setText(String.valueOf(m_kisasuoritus.getSuoritusID()));
            txtLuokka.setText(m_kisasuoritus.getLuokka());
            txtKokonaispisteet.setText(m_kisasuoritus.getKokonaispisteet());
            txtSuoritusaika.setText(m_kisasuoritus.getSuoritusaika());
            txtSijoitus.setText(m_kisasuoritus.getSijoitus());
            txtKoulutustunnus.setText(m_kisasuoritus.getKoulutustunnus());
            txtKeskTaiHyl.setText(m_kisasuoritus.getKeskeytysTaiHylkays());
            txtKisanID.setText(String.valueOf(m_kisasuoritus.getKisaID()));
            txtKoiranID.setText(String.valueOf(m_kisasuoritus.getKoiraID()));
        }

    }

    public void lisaaTiedot() {

        boolean kisasuoritus_lisatty = true;
        m_kisasuoritus = null;
        try {
            m_kisasuoritus = kisasuoritus.haeKilpailusuoritus(m_conn, Integer.parseInt(txtSuoritusID.getText()));
        } catch (SQLException se) {

            kisasuoritus_lisatty = false;
            se.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailusuorituksen tietojen lisääminen ei onnistu.");
            alert.showAndWait();

        } catch (Exception e) {

            kisasuoritus_lisatty = false;
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailusuorituksen tietojen lisääminen ei onnistu.");
            alert.showAndWait();
        }
        if (m_kisasuoritus.getLuokka() != null) {

            kisasuoritus_lisatty = false;
            txtSuoritusID.setText(String.valueOf(m_kisasuoritus.getSuoritusID()));
            txtLuokka.setText(m_kisasuoritus.getLuokka());
            txtKokonaispisteet.setText(m_kisasuoritus.getKokonaispisteet());
            txtSuoritusaika.setText(m_kisasuoritus.getSuoritusaika());
            txtSijoitus.setText(m_kisasuoritus.getSijoitus());
            txtKoulutustunnus.setText(m_kisasuoritus.getKoulutustunnus());
            txtKeskTaiHyl.setText(m_kisasuoritus.getKeskeytysTaiHylkays());
            txtKisanID.setText(String.valueOf(m_kisasuoritus.getKisaID()));
            txtKoiranID.setText(String.valueOf(m_kisasuoritus.getKoiraID()));

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen lisaaminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailusuoritus on jo olemassa.");
            alert.showAndWait();

        } else {

            m_kisasuoritus.setSuoritusID(Integer.parseInt(txtSuoritusID.getText()));
            m_kisasuoritus.setLuokka(txtLuokka.getText());
            m_kisasuoritus.setKokonaispisteet(txtKokonaispisteet.getText());
            m_kisasuoritus.setSuoritusaika(txtSuoritusaika.getText());
            m_kisasuoritus.setSijoitus(txtSijoitus.getText());
            m_kisasuoritus.setKoulutustunnus(txtKoulutustunnus.getText());
            m_kisasuoritus.setKeskeytysTaiHylkays(txtKeskTaiHyl.getText());
            m_kisasuoritus.setKisaID(Integer.parseInt(txtKisanID.getText()));
            m_kisasuoritus.setKoiraID(Integer.parseInt(txtKoiranID.getText()));
            try {

                m_kisasuoritus.lisaaKilpailusuoritus(m_conn);
            } catch (SQLException se) {

                kisasuoritus_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Kilpailusuorituksen tietojen lisääminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Kilpailusuorituksen tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                se.printStackTrace();
            } catch (Exception e) {

                kisasuoritus_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Kilpailusuorituksen tietojen lisaaminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Kilpailusuorituksen tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                e.printStackTrace();
            } finally {
                if (kisasuoritus_lisatty == true) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Kilpailusuorituksen tietojen lisaaminen");
                    alert.setHeaderText("Toiminto ok.");
                    alert.setContentText("Kilpailusuorituksen tiedot lisatty tietokantaan.");
                    alert.showAndWait();

                }
            }

        }

    }

    public void muutaTiedot() {

        boolean kisasuoritus_muutettu = true;

        m_kisasuoritus.setLuokka(txtLuokka.getText());
        m_kisasuoritus.setKokonaispisteet(txtKokonaispisteet.getText());
        m_kisasuoritus.setSuoritusaika(txtSuoritusaika.getText());
        m_kisasuoritus.setSijoitus(txtSijoitus.getText());
        m_kisasuoritus.setKoulutustunnus(txtKoulutustunnus.getText());
        m_kisasuoritus.setKeskeytysTaiHylkays(txtKeskTaiHyl.getText());
        m_kisasuoritus.setKisaID(Integer.parseInt(txtKisanID.getText()));
        m_kisasuoritus.setKoiraID(Integer.parseInt(txtKoiranID.getText()));

        try {

            m_kisasuoritus.muutaKilpailusuoritusta(m_conn);
        } catch (SQLException se) {

            kisasuoritus_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailusuorituksen tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            kisasuoritus_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailusuorituksen tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (kisasuoritus_muutettu == true) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Kilpailusuorituksen tietojen muuttaminen");
                alert.setHeaderText("Toiminto ok.");
                alert.setContentText("Kilpailusuorituksen tiedot muutettu.");
                alert.showAndWait();

            }
        }

    }

    public void poistaTiedot() {

        m_kisasuoritus = null;
        boolean kisasuoritus_poistettu = false;

        try {
            m_kisasuoritus = kisasuoritus.haeKilpailusuoritus(m_conn, Integer.parseInt(txtKoiranID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailusuorituksen tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Kilpailusuorituksen tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        }
        if (m_kisasuoritus.getLuokka() == null) {

            txtLuokka.setText("");
            txtKokonaispisteet.setText("");
            txtSuoritusaika.setText("");
            txtSijoitus.setText("");
            txtKoulutustunnus.setText("");
            txtKeskTaiHyl.setText("");
            txtKisanID.setText("");
            txtKoiranID.setText("");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen poisto");
            alert.setHeaderText("Virhe");
            alert.setContentText("Kilpailusuoritusta ei loydy.");
            alert.showAndWait();

            return;
        } else {

            txtLuokka.setText(m_kisasuoritus.getLuokka());
            txtKokonaispisteet.setText(m_kisasuoritus.getKokonaispisteet());
            txtSuoritusaika.setText(m_kisasuoritus.getSuoritusaika());
            txtSijoitus.setText(m_kisasuoritus.getSijoitus());
            txtKoulutustunnus.setText(m_kisasuoritus.getKoulutustunnus());
            txtKeskTaiHyl.setText(m_kisasuoritus.getKeskeytysTaiHylkays());
            txtKisanID.setText(String.valueOf(m_kisasuoritus.getKisaID()));
            txtKoiranID.setText(String.valueOf(m_kisasuoritus.getKoiraID()));
        }
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Kilpailusuorituksen tietojen poisto");
            alert.setHeaderText("Vahvista");
            alert.setContentText("Haluatko todella poistaa kilpailusuorituksen?");

            Optional<ButtonType> vastaus = alert.showAndWait();

            if (vastaus.get() == ButtonType.OK) {
                m_kisasuoritus.poistaKilpailusuoritus(m_conn);
                kisasuoritus_poistettu = true;
            }
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Kilpailusuorituksen tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kilpailusuorituksen tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Kilpailusuorituksen tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (kisasuoritus_poistettu == true) {
                txtSuoritusID.setText("");
                txtLuokka.setText("");
                txtKokonaispisteet.setText("");
                txtSuoritusaika.setText("");
                txtSijoitus.setText("");
                txtKoulutustunnus.setText("");
                txtKeskTaiHyl.setText("");
                txtKisanID.setText("");
                txtKoiranID.setText("");

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Kilpailusuorituksen tietojen poisto");
                alert.setHeaderText("Tulos:");
                alert.setContentText("Kilpailusuorituksen tiedot poistettu tietokannasta.");
                alert.showAndWait();

                m_kisasuoritus = null;
            }
        }

    }
}

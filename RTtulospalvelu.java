
package rttulospalvelu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * RTtulopalvelu-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class RTtulospalvelu extends Application {


    private static Connection openConnection(String connString) throws SQLException {
        Connection con = DriverManager.getConnection(connString);
        System.out.println("\t>> Yhteys ok");

        return con;
    }

  
    private static void closeConnection(Connection c) throws SQLException {
        if (c != null) {
            c.close();
        }
        System.out.println("\t>> Tietokantayhteys suljettu");
    }

   
    private static void createDatabase(Connection c, String db) throws SQLException {

        Statement stmt = c.createStatement();
        stmt.executeQuery("DROP DATABASE IF EXISTS " + db); 

        System.out.println("\t>> Tietokanta " + db + " poistettu");

        stmt.executeQuery("CREATE DATABASE " + db); 

        System.out.println("\t>> Tietokanta " + db + " luotu");

        stmt.executeQuery("USE " + db); 

        System.out.println("\t>> Kaytetaan tietokantaa " + db);
    }

    private static void createTable(Connection c, String sql) throws SQLException {

        Statement stmt = c.createStatement();
        stmt.executeQuery(sql);
        System.out.println("\t>> Taulu luotu");

    }

    private static void addLaji(Connection c, String lajinNimi) throws SQLException {

        PreparedStatement ps = c.prepareStatement(
               
                "INSERT INTO laji (lajinNimi) "
                + " VALUES(?)"
        );

        ps.setString(1, lajinNimi);
        ps.execute();
        System.out.println("\t>> Lisatty " + lajinNimi);

    }

    private static void addJarjestaja(Connection c, String seuranNimi, String koetoimitsija, String kennelpiiri) throws SQLException {

        PreparedStatement ps = c.prepareStatement(
               
                "INSERT INTO jarjestaja (seuranNimi, koetoimitsija, kennelpiiri) "
                + " VALUES(?, ?, ?)"
        );

        ps.setString(1, seuranNimi);
        ps.setString(2, koetoimitsija);
        ps.setString(3, kennelpiiri);
        ps.execute();
        System.out.println("\t>> Lisatty " + seuranNimi + ", " + koetoimitsija + ", " + kennelpiiri);

    }
    
    private static void addOmistaja(Connection c, String Etunimi, String Sukunimi) throws SQLException {

        PreparedStatement ps = c.prepareStatement(
           
                "INSERT INTO omistaja (Etunimi, Sukunimi) "
                + " VALUES(?, ?)"
        );

        ps.setString(1, Etunimi);
        ps.setString(2, Sukunimi);
        ps.execute();
        System.out.println("\t>> Lisatty " + Etunimi + ", " + Sukunimi);

    }
    
    private static void addKisat(Connection c, String pvm, String osallistujamaara, String paikkakunta, String tuomari, int jarjestajaID, int lajinID) throws SQLException {

        PreparedStatement ps = c.prepareStatement(
              
                "INSERT INTO kisat (pvm, osallistujamaara, paikkakunta, tuomari, jarjestajaID, lajinID) "
                + " VALUES(?, ?, ?, ?, ?, ?)"
        );

        ps.setString(1, pvm);
        ps.setString(2, osallistujamaara);
        ps.setString(3, paikkakunta);
        ps.setString(4, tuomari);
        ps.setInt(5, jarjestajaID);
        ps.setInt(6, lajinID);
        ps.execute();
        System.out.println("\t>> Lisatty " + pvm + ", " + osallistujamaara + ", " + paikkakunta + ", " + tuomari + ", " + jarjestajaID + ", " + lajinID);

    }
    
    private static void addKoira(Connection c, String virallinenNimi, String rekNro, String syntymaaika, String rotu, String sakakorkeusluokka, int omistajaID) throws SQLException {

        PreparedStatement ps = c.prepareStatement(
              
                "INSERT INTO koira (virallinenNimi, rekNro, syntymaaika, rotu, sakakorkeusluokka, omistajaID) "
                + " VALUES(?, ?, ?, ?, ?, ?)"
        );

        ps.setString(1, virallinenNimi);
        ps.setString(2, rekNro);
        ps.setString(3, syntymaaika);
        ps.setString(4, rotu);
        ps.setString(5, sakakorkeusluokka);
        ps.setInt(6, omistajaID);
        ps.execute();
        System.out.println("\t>> Lisatty " + virallinenNimi + ", " + rekNro + ", " + syntymaaika + ", " + rotu + ", " + sakakorkeusluokka + ", " + omistajaID);

    }
    
    private static void addKisasuoritus(Connection c, String luokka, String kokonaispisteet, String suoritusaika, String sijoitus, String koulutustunnus, String keskeytysTaiHylkays, int kisaID, int koiraID) throws SQLException {

        PreparedStatement ps = c.prepareStatement(
              
                "INSERT INTO kisasuoritus (luokka, kokonaispisteet, suoritusaika, sijoitus, koulutustunnus, keskeytysTaiHylkays, kisaID, koiraID) "
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
        );

        ps.setString(1, luokka);
        ps.setString(2, kokonaispisteet);
        ps.setString(3, suoritusaika);
        ps.setString(4, sijoitus);
        ps.setString(5, koulutustunnus);
        ps.setString(6, keskeytysTaiHylkays);
        ps.setInt(7, kisaID);
        ps.setInt(8, koiraID);
        ps.execute();
        System.out.println("\t>> Lisatty " + luokka + ", " + kokonaispisteet + ", " + suoritusaika + ", " + sijoitus + ", " + koulutustunnus + ", " + keskeytysTaiHylkays + ", " + kisaID + ", " + koiraID);

    }
    
    /**
     *
     * @param stage
     * @throws Exception
     */
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));

        Scene scene = new Scene(root);

        stage.setTitle("Rally-tokokisojen tulospalvelu");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws SQLException {

        Connection conn = openConnection("");

        createDatabase(conn, "rt_tulospalvelu");

        createTable(conn,
                "CREATE TABLE laji ("
                + "lajinID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                + "lajinNimi VARCHAR(10) NOT NULL)"
        );

        createTable(conn,
                "CREATE TABLE jarjestaja ("
                + "jarjestajaID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                + "seuranNimi VARCHAR(40) NOT NULL,"
                + "koetoimitsija VARCHAR(40) NOT NULL,"
                + "kennelpiiri VARCHAR(40) NOT NULL)"
        );

        createTable(conn,
                "CREATE TABLE omistaja ("
                + "omistajaID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                + "Etunimi VARCHAR(20) NOT NULL,"
                + "Sukunimi VARCHAR(20) NOT NULL)"
        );

        createTable(conn,
                "CREATE TABLE kisat ("
                + "kisaID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                + "pvm VARCHAR(10) NOT NULL,"
                + "osallistujamaara VARCHAR(3) NOT NULL,"
                + "paikkakunta VARCHAR(30) NOT NULL,"
                + "tuomari VARCHAR(40) NOT NULL,"
                + "jarjestajaID INT NOT NULL,"
                + "lajinID INT NOT NULL,"
                + "FOREIGN KEY (jarjestajaID) REFERENCES jarjestaja (jarjestajaID),"
                + "FOREIGN KEY (lajinID) REFERENCES laji (lajinID))"
        );

        createTable(conn,
                "CREATE TABLE koira ("
                + "koiraID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                + "virallinenNimi VARCHAR(40) NOT NULL,"
                + "rekNro VARCHAR(15) NOT NULL,"
                + "syntymaaika VARCHAR(10) NOT NULL,"
                + "rotu VARCHAR(40) NOT NULL,"
                + "sakakorkeusluokka VARCHAR(10) NOT NULL,"
                + "omistajaID INT NOT NULL,"
                + "FOREIGN KEY (omistajaID) REFERENCES omistaja(omistajaID))"
        );

        createTable(conn,
                "CREATE TABLE kisasuoritus ("
                + "suoritusID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                + "luokka VARCHAR(20) NOT NULL,"
                + "kokonaispisteet VARCHAR(3) NOT NULL,"
                + "suoritusaika VARCHAR(11),"
                + "sijoitus VARCHAR(2),"
                + "koulutustunnus VARCHAR(4),"
                + "keskeytysTaiHylkays VARCHAR(40),"
                + "kisaID INT NOT NULL,"
                + "koiraID INT NOT NULL,"
                + "FOREIGN KEY (kisaID) REFERENCES kisat(kisaID),"
                + "FOREIGN KEY (koiraID) REFERENCES koira(koiraID))"
        );

        closeConnection(conn);
        
        launch(args);

    }

}

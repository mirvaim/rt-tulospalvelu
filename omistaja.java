package rttulospalvelu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Omistaja-luokan metodeita.
 *
 * @author Mirva I
 * @version 1.00 20/3/2022
 */
public class omistaja {

    private int omistajaID;
    private String Etunimi;
    private String Sukunimi;

    /**
     * @return Palauttaa omistajan ID:n
     */
    public int getOmistajaID() {
        return omistajaID;
    }

    /**
     * @param aOmistajaID Omistajan ID
     */
    public void setOmistajaID(int aOmistajaID) {
        omistajaID = aOmistajaID;
    }

    /**
     * @return Palauttaa etunimen
     */
    public String getEtunimi() {
        return Etunimi;
    }

    /**
     * @param aEtunimi Etunimi
     */
    public void setEtunimi(String aEtunimi) {
        Etunimi = aEtunimi;
    }

    /**
     * @return Palauttaa sukunimen
     */
    public String getSukunimi() {
        return Sukunimi;
    }

    /**
     * @param aSukunimi Sukunimi
     */
    public void setSukunimi(String aSukunimi) {
        Sukunimi = aSukunimi;
    }

    @Override
    public String toString() {
        return (omistajaID + " " + Etunimi + " " + Sukunimi);
    }

    public static omistaja haeOmistaja(Connection connection, int id) throws SQLException, Exception {

        String sql = "SELECT omistajaID, Etunimi, Sukunimi "
                + " FROM omistaja WHERE omistajaID = ?";
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {

            lause = connection.prepareStatement(sql);
            lause.setInt(1, id);

            tulosjoukko = lause.executeQuery();
            if (tulosjoukko == null) {
                throw new Exception("Omistajaa ei loydy");
            }
        } catch (SQLException se) {

            throw se;
        } catch (Exception e) {

            throw e;
        }

        omistaja omistajaOlio = new omistaja();

        try {
            if (tulosjoukko.next() == true) {

                omistajaOlio.setOmistajaID(tulosjoukko.getInt("omistajaID"));
                omistajaOlio.setEtunimi(tulosjoukko.getString("Etunimi"));
                omistajaOlio.setSukunimi(tulosjoukko.getString("Sukunimi"));
            }

        } catch (SQLException e) {
            throw e;
        }

        return omistajaOlio;
    }

    public int lisaaOmistaja(Connection connection) throws SQLException, Exception { 

        String sql = "SELECT omistajaID"
                + " FROM omistaja WHERE omistajaID = ?"; 
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;

        try {
   
            lause = connection.prepareStatement(sql);
            lause.setInt(1, getOmistajaID()); 
        
            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == true) { 
                throw new Exception("Omistaja on jo olemassa");
            }
        } catch (SQLException se) {
       
            throw se;
        } catch (Exception e) {
       
            throw e;
        }
    
        sql = "INSERT INTO omistaja "
                + "(omistajaID, Etunimi, Sukunimi) "
                + " VALUES (?, ?, ?)";
   
        lause = null;
        try {
     
            lause = connection.prepareStatement(sql);
       
            lause.setInt(1, getOmistajaID());
            lause.setString(2, getEtunimi());
            lause.setString(3, getSukunimi());
           
            int lkm = lause.executeUpdate();
       
            if (lkm == 0) {
                throw new Exception("Omistajan lisaaminen ei onnistu");
            }
        } catch (SQLException se) {
      
            throw se;
        } catch (Exception e) {
        
            throw e;
        }
        return 0;
    }

    public int muutaOmistaja(Connection connection) throws SQLException, Exception { 

        String sql = "SELECT omistajaID"
                + " FROM omistaja WHERE omistajaID = ?"; 
        ResultSet tulosjoukko = null;
        PreparedStatement lause = null;
        try {
   
            lause = connection.prepareStatement(sql);
            lause.setInt(1, getOmistajaID()); 
 
            tulosjoukko = lause.executeQuery();
            if (tulosjoukko.next() == false) { 
                throw new Exception("Omistajaa ei loydy tietokannasta");
            }
        } catch (SQLException se) {
       
            throw se;
        } catch (Exception e) {
      
            throw e;
        }
       
        sql = "UPDATE omistaja "
                + "SET Etunimi = ?, Sukunimi = ? "
                + " WHERE omistajaID = ?";

        lause = null;
        try {
       
            lause = connection.prepareStatement(sql);

        
            lause.setString(1, getEtunimi());
            lause.setString(2, getSukunimi());
       
            lause.setInt(3, getOmistajaID());
    
            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Omistajan muuttaminen ei onnistu");
            }
        } catch (SQLException se) {
        
            throw se;
        } catch (Exception e) {
       
            throw e;
        }
        return 0; 
    }


    public int poistaOmistaja(Connection connection) throws SQLException, Exception { 

   
        String sql = "DELETE FROM omistaja WHERE omistajaID = ?";
        PreparedStatement lause = null;
        try {
     
            lause = connection.prepareStatement(sql);
      
            lause.setInt(1, getOmistajaID());
         
            int lkm = lause.executeUpdate();
            if (lkm == 0) {
                throw new Exception("Omistajan poistaminen ei onnistu");
            }
        } catch (SQLException se) {
            
            throw se;
        } catch (Exception e) {
        
            throw e;
        }
        return 0; 
    }

}

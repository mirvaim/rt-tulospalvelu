package rttulospalvelu;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Koira Controller
 *
 * @author Mirva I
 * @version 1.00 21/3/2022
 */
public class FXMLkoiraController implements Initializable {

    private koira m_koirat = new koira();
    private Connection m_conn;

    @FXML
    private TextField txtKoiranID;
    @FXML
    private TextField txtVirallinenNimi;
    @FXML
    private TextField txtRekNro;
    @FXML
    private TextField txtSyntAika;
    @FXML
    private TextField txtRotu;
    @FXML
    private TextField txtSakakorkeusluokka;
    @FXML
    private TextField txtOmistajanID;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            yhdista();
        } catch (SQLException se) {

            System.out.println("Tapahtui tietokantavirhe tietokantaa avattaessa.");

        } catch (Exception e) {

            System.out.println("Tapahtui JDBCvirhe tietokantaa avattaessa.");

        }
    }

    public void yhdista() throws SQLException, Exception {
        m_conn = null;
        String url = "";
        try {

            m_conn = DriverManager.getConnection(url, "", "");

        } catch (SQLException e) {
            m_conn = null;
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public void sulje_kanta() throws SQLException, Exception {

        try {

            m_conn.close();
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    @FXML
    private void btn_backToMenu(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("RTtulospalveluView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void btn_lisaaKoira(ActionEvent event) {
        lisaaTiedot();
    }

    @FXML
    private void btn_muokkaaKoiraa(ActionEvent event) {
        muutaTiedot();
    }

    @FXML
    private void btn_poistaKoira(ActionEvent event) {
        poistaTiedot();
    }

    @FXML
    private void btn_haeKoira(ActionEvent event) {
        haeTiedot();
    }

    public void haeTiedot() {

        m_koirat = null;

        try {
            m_koirat = koira.haeKoira(m_conn, Integer.parseInt(txtKoiranID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

        }
        if (m_koirat.getVirallinenNimi() == null) {

            txtKoiranID.setText("");
            txtVirallinenNimi.setText("");
            txtRekNro.setText("");
            txtSyntAika.setText("");
            txtRotu.setText("");
            txtSakakorkeusluokka.setText("");
            txtOmistajanID.setText("");

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen hakeminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

        } else {

            txtKoiranID.setText(String.valueOf(m_koirat.getKoiraID()));
            txtVirallinenNimi.setText(m_koirat.getVirallinenNimi());
            txtRekNro.setText(m_koirat.getRekNro());
            txtSyntAika.setText(m_koirat.getSyntymaaika());
            txtRotu.setText(m_koirat.getRotu());
            txtSakakorkeusluokka.setText(m_koirat.getSakakorkeusluokka());
            txtOmistajanID.setText(String.valueOf(m_koirat.getOmistajaID()));
        }

    }

    public void lisaaTiedot() {

        boolean koira_lisatty = true;
        m_koirat = null;
        try {
            m_koirat = koira.haeKoira(m_conn, Integer.parseInt(txtKoiranID.getText()));
        } catch (SQLException se) {

            koira_lisatty = false;
            se.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Koiran tietojen lisääminen ei onnistu.");
            alert.showAndWait();

        } catch (Exception e) {

            koira_lisatty = false;
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen lisaaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Koiran tietojen lisääminen ei onnistu.");
            alert.showAndWait();
        }
        if (m_koirat.getVirallinenNimi() != null) {

            koira_lisatty = false;
            txtKoiranID.setText(String.valueOf(m_koirat.getKoiraID()));
            txtVirallinenNimi.setText(m_koirat.getVirallinenNimi());
            txtRekNro.setText(m_koirat.getRekNro());
            txtSyntAika.setText(m_koirat.getSyntymaaika());
            txtRotu.setText(m_koirat.getRotu());
            txtSakakorkeusluokka.setText(m_koirat.getSakakorkeusluokka());
            txtOmistajanID.setText(String.valueOf(m_koirat.getOmistajaID()));

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen lisaaminen");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koira on jo olemassa.");
            alert.showAndWait();

        } else {

            m_koirat.setKoiraID(Integer.parseInt(txtKoiranID.getText()));
            m_koirat.setVirallinenNimi(txtVirallinenNimi.getText());
            m_koirat.setRekNro(txtRekNro.getText());
            m_koirat.setSyntymaaika(txtSyntAika.getText());
            m_koirat.setRotu(txtRotu.getText());
            m_koirat.setSakakorkeusluokka(txtSakakorkeusluokka.getText());
            m_koirat.setOmistajaID(Integer.parseInt(txtOmistajanID.getText()));
            try {

                m_koirat.lisaaKoira(m_conn);
            } catch (SQLException se) {

                koira_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Koiran tietojen lisääminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Koiran tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                se.printStackTrace();
            } catch (Exception e) {

                koira_lisatty = false;

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Koiran tietojen lisaaminen");
                alert.setHeaderText("Tietokantavirhe");
                alert.setContentText("Koiran tietojen lisääminen ei onnistu.");
                alert.showAndWait();

                e.printStackTrace();
            } finally {
                if (koira_lisatty == true) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Koiran tietojen lisaaminen");
                    alert.setHeaderText("Toiminto ok.");
                    alert.setContentText("Koiran tiedot lisatty tietokantaan.");
                    alert.showAndWait();

                }
            }

        }

    }

    public void muutaTiedot() {

        boolean koira_muutettu = true;

        m_koirat.setVirallinenNimi(txtVirallinenNimi.getText());
        m_koirat.setRekNro(txtRekNro.getText());
        m_koirat.setSyntymaaika(txtSyntAika.getText());
        m_koirat.setRotu(txtRotu.getText());
        m_koirat.setSakakorkeusluokka(txtSakakorkeusluokka.getText());
        m_koirat.setOmistajaID(Integer.parseInt(txtOmistajanID.getText()));

        try {

            m_koirat.muutaKoira(m_conn);
        } catch (SQLException se) {

            koira_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Koiran tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            koira_muutettu = false;

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen muuttaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Koiran tietojen muuttaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (koira_muutettu == true) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Koiran tietojen muuttaminen");
                alert.setHeaderText("Toiminto ok.");
                alert.setContentText("Koiran tiedot muutettu.");
                alert.showAndWait();

            }
        }

    }

    public void poistaTiedot() {

        m_koirat = null;
        boolean koira_poistettu = false;

        try {
            m_koirat = koira.haeKoira(m_conn, Integer.parseInt(txtKoiranID.getText()));
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Koiran tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen poistaminen");
            alert.setHeaderText("Tietokantavirhe");
            alert.setContentText("Koiran tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        }
        if (m_koirat.getVirallinenNimi() == null) {

            txtVirallinenNimi.setText("");
            txtRekNro.setText("");
            txtSyntAika.setText("");
            txtRotu.setText("");
            txtSakakorkeusluokka.setText("");
            txtOmistajanID.setText("");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen poisto");
            alert.setHeaderText("Virhe");
            alert.setContentText("Koiraa ei loydy.");
            alert.showAndWait();

            return;
        } else {

            txtVirallinenNimi.setText(m_koirat.getVirallinenNimi());
            txtRekNro.setText(m_koirat.getRekNro());
            txtSyntAika.setText(m_koirat.getSyntymaaika());
            txtRotu.setText(m_koirat.getRotu());
            txtSakakorkeusluokka.setText(m_koirat.getSakakorkeusluokka());
            txtOmistajanID.setText(String.valueOf(m_koirat.getOmistajaID()));
        }
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Koiran tietojen poisto");
            alert.setHeaderText("Vahvista");
            alert.setContentText("Haluatko todella poistaa koiran tiedot?");

            Optional<ButtonType> vastaus = alert.showAndWait();

            if (vastaus.get() == ButtonType.OK) {
                m_koirat.poistaKoira(m_conn);
                koira_poistettu = true;
            }
        } catch (SQLException se) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Koiran tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            se.printStackTrace();
        } catch (Exception e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Koiran tietojen poisto");
            alert.setHeaderText("Tulos:");
            alert.setContentText("Koiran tietojen poistaminen ei onnistu.");
            alert.showAndWait();

            e.printStackTrace();
        } finally {
            if (koira_poistettu == true) {
                txtKoiranID.setText("");
                txtVirallinenNimi.setText("");
                txtRekNro.setText("");
                txtSyntAika.setText("");
                txtRotu.setText("");
                txtSakakorkeusluokka.setText("");
                txtOmistajanID.setText("");

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Koiran tietojen poisto");
                alert.setHeaderText("Tulos:");
                alert.setContentText("Koiran tiedot poistettu tietokannasta.");
                alert.showAndWait();

                m_koirat = null;
            }
        }

    }

}
